## How it works:

Just add your thumbnails to the thumbnails section in the following format:

```html
<article>
    <a class="thumbnail" href="path/to/fullsize.jpg">
        <img src="path/to/thumbnail.jpg" alt="" />
    </a>
    <h2>Title</h2>
    <p>Description.</p>
</article>
```

And that's it. Lens will figure out the rest.


## The "data-position" attribute:

As a full screen experience, the viewer will be subject to changes in its size and,
consequently, its aspect ratio. Since your full size images are basically applied as
backgrounds to the viewer itself, this means they'll probably (okay, definitely) get
cropped. All is not lost, however, as you can use the optional "data-position" attribute
to control how the full size image is positioned within the viewer. To do this, simply
add it to your thumbnail's `<a>` element and set it to any valid "background-position"
value. For example, this:

```html
<a class="thumbnail" href="path/to/fullsize.jpg" data-position="top left">...</a>
```

... positions this particular full size image in the top left corner of the viewer (as
opposed to its center, the default), effectively limiting cropping to everything but
the top left corner.


## Keyboard shortcuts:

Lens is set up to respond to the following keyboard shortcuts:

- Left Arrow: Go to previous image.
- Right Arrow: Go to next image.
- Up Arrow: Go to image above the current one in the thumbnails section.
- Down Arrow: Go to image below the current one in the thumbnails section.
- Space: Go to next image.
- Escape: Toggle the main wrapper.
